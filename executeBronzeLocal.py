
from Libraries.SubmitScores.Bronze import main

DEFAULT_MOUNT_PATH = "dbfs:/mnt/subscNov"
DEFAULT_WRITE_PATH = "dbfs:/user/Michael.Paris@act.org/sensorDataEvents2019Nov.parquet"

import argparse

args = False

parser = argparse.ArgumentParser()
parser.add_argument("--mount_path", default=DEFAULT_MOUNT_PATH,
                help="dbfs mount to read log streams from")
parser.add_argument("--write_path", default=DEFAULT_WRITE_PATH,
                help="bronze table location to write the data to")
args = parser.parse_args()

mnt_path = args.mount_path
wrt_path = args.write_path


main(mnt_path,wrt_path)





