# evaluatenotebookruns.py
import unittest
import xmlrunner
import json
import glob
import os


#

class TestJobOutput(unittest.TestCase):

  test_output_path = os.getenv('OUTFILEPATH')

  def test_performance(self):
      path = self.test_output_path
      statuses = []
      print(path)


      for filename in glob.glob(os.path.join(path, '*.json')):
          print('Evaluating: ' + filename)
          with open(filename) as f:
            data = json.load(f)

          duration = data['execution_duration']
          
          if duration > 100000:
              status = 'FAILED'
          else:
              status = 'SUCCESS'

          statuses.append(status)

      self.assertFalse('FAILED' in statuses)


  def test_job_run(self):
      path = self.test_output_path
      test_results = glob.glob(os.path.join(path, '*.json'))
      self.assertTrue( len(test_results) > 0 )
      statuses = []


      for filename in test_results:
          print('Evaluating: ' + filename)
          with open(filename) as f:
            data = json.load(f)

          print("dump here")
          print("run_name: {}".format(data['run_name']))
          print("run_page_url: {}".format(data['run_page_url']))
          status = data['state']['result_state']
          state = data['state']['life_cycle_state']
          statuses.append(status)
          statuses.append(state)

      self.assertFalse('FAILED' in statuses)
      self.assertFalse('INTERNAL_ERROR' in statuses)

if __name__ == '__main__':
  unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))