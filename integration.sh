python $SCRIPTPATH/executenotebook.py \
    --workspace=$DATABRICKS_ADDRESS \
    --token=$DATABRICKS_API_TOKEN \
    --clusterid=$DATABRICKS_CLUSTER_ID \
    --localpath=$NOTEBOOKPATH"VALIDATION" \
    --workspacepath=$WORKSPACEPATH"VALIDATION" \
    --outfilepath=$OUTFILEPATH

#python -m pytest --junit-xml=${TESTRESULTPATH}/TEST-notebookout.xml ${SCRIPTPATH}/evaluatenotebookruns.py || true
#python -m pytest --junit-xml=${TESTRESULTPATH}/TEST-notebookout.xml ${SCRIPTPATH}/evaluatenotebookruns.py
python -m unittest -v ${SCRIPTPATH}/evaluatenotebookruns.py
    