# Databricks notebook source
log4jLogger = sc._jvm.org.apache.log4j
log = log4jLogger.LogManager.getLogger(__name__)
log.info("Logger init")
# COMMAND ----------

DEFAULT_MOUNT_PATH = "dbfs:/mnt/subscNovDev"
DEFAULT_WRITE_PATH = "dbfs:/user/Michael.Paris@act.org/sensorDataEvents2019NovDev.parquet"


# COMMAND ----------

try:
  mnt_path = dbutils.widgets.get("mount_path")
  wrt_path = dbutils.widgets.get("write_path")
  log.warn("jobBronze notebook args parsed")

except:
  mnt_path = DEFAULT_MOUNT_PATH
  wrt_path = DEFAULT_WRITE_PATH
  log.warn("jobBronze notebook args not parsed; using DEFAULT")

log.warn("jobBronze running with mount:{} write:{}".format(mnt_path,wrt_path))

# COMMAND ----------

from SubmitScores.Bronze import Pipeline

# COMMAND ----------
df = Pipeline.extract(spark, mnt_path)
transformedDF = Pipeline.transform(spark, df)
Pipeline.load(spark, wrt_path, transformedDF)


# COMMAND ----------

