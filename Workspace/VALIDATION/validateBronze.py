# Databricks notebook source

log4jLogger = sc._jvm.org.apache.log4j
log = log4jLogger.LogManager.getLogger(__name__)
log.info("Logger init")

# COMMAND ----------

mount_path_arg = dbutils.widgets.get("mount_path")
write_path_arg = dbutils.widgets.get("write_path")

final_count_arg = dbutils.widgets.get("final_count")




# COMMAND ----------

job_params = {
    "mount_path": mount_path_arg,
    "write_path": write_path_arg
}

print(job_params)
print("final count arg: {}".format(final_count_arg))
log.warn(job_params)
log.warn(final_count_arg)



# COMMAND ----------
jobPath = "../jobBronze"
dbutils.notebook.run(jobPath, 1000000, job_params )



# COMMAND ----------
# Get result table

finalDF = (
  spark.read.parquet(job_params['write_path'])
)
# COMMAND ----------
# Final Count Test
try: 
  assert(finalDF.count() == int(final_count_arg))
except:
  print("expected: {}".format(final_count_arg))
  print("received: {}".format(finalDF.count()))
  assert False
# COMMAND ----------
# Final Schema test
from SubmitScores.StructTypes import SubmitScoresTypes

expectedSchema = SubmitScoresTypes.tableBronzeType
expectedDF = spark.createDataFrame([],expectedSchema)

extraFields = set(finalDF.schema) - set(expectedDF.schema)
reqFields = set(expectedDF.schema) - set(finalDF.schema)

try:
  assert bool(extraFields) == False
  assert bool(reqFields) == False
except:
  print("expected: ")
  expectedDF.printSchema()
  print("received: ") 
  finalDF.printSchema()
  assert False
