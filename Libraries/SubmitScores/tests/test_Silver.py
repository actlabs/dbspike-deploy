from pyspark.sql import SparkSession
from pyspark.sql.functions import col, from_json
from pyspark.sql.types import TimestampType, IntegerType, StructField


import pytest
import os
import pandas as pd

from .context import SubmitScores
from SubmitScores.Silver import Transformations
from SubmitScores.StructTypes import SubmitScoresTypes


spark = SparkSession.builder.getOrCreate()


def mock_extract():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    fixture_path = os.path.join(dir_path, "fixtures/bronzeFixture.csv")
    colnames = ["logTimestamp","sendTime","id","actor","object","eventTime","group","generated"]
    pDF = pd.read_csv(fixture_path, names=colnames )


    
    sDF = spark.createDataFrame(pDF)
    jsoncols = ["actor", "object", "group", "generated"]
    

    bronzeDF = (
        sDF
            .select(
                col("logTimestamp").cast(TimestampType()),
                col("sendTime").cast(TimestampType()),
                col("id"),
                from_json(col("actor"), SubmitScoresTypes.actorStruct).alias("actor"),
                from_json(col("object"), SubmitScoresTypes.objectStruct).alias("object"),
                col("eventTime").cast(TimestampType()),
                from_json(col("group"), SubmitScoresTypes.groupStruct).alias("group"),
                from_json(col("generated"), SubmitScoresTypes.generatedStruct).alias("generated")
            )
    )


    return bronzeDF



def test_add_schema():


    expectedSchema = (
        SubmitScoresTypes.tableBronzeType
            .add(StructField("sendYear", IntegerType(), True))
            .add(StructField("sendMonth", IntegerType(), True))
            .add(StructField("logDay", IntegerType(), True))
    )
    checkDF = spark.createDataFrame([],expectedSchema)


    mockBronzeDF = mock_extract()
    mockBronzeDateDF = Transformations.addDateColumns(mockBronzeDF)

    diffSchema = set(mockBronzeDateDF.schema) - set(checkDF.schema)

    assert bool(diffSchema) == False