from pyspark.sql import SparkSession

import pytest
import os

from .context import SubmitScores
from SubmitScores.Bronze import Transformations


spark = SparkSession.builder.getOrCreate()


def raw_mock_extract():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    fixture_path = os.path.join(dir_path, "fixtures/rawSubset.txt")

    with open(fixture_path, "r") as f:
        lines = f.readlines()

    
    rdd = spark.sparkContext.parallelize([ (line,) for line in lines])


    schema = ["value"]

    mockDF = spark.createDataFrame( rdd , schema)

    return mockDF


def test_event_filter():
    rawDF = raw_mock_extract()
    splitDF = Transformations.splitRADEvents(rawDF)
    eventDF = Transformations.getRADEvents(splitDF)

    eventCount = eventDF.count()

    assert eventCount == 6







