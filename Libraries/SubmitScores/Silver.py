from pyspark.sql import SparkSession
from pyspark.sql.functions import year, month, dayofmonth, col, window

from .StructTypes import RADTypes, SubmitScoresTypes

class Transformations:

    def addDateColumns(df):
        return (
            df
            .withColumn("sendYear", year(col("sendTime")))
            .withColumn("sendMonth", month(col("sendTime")))
            .withColumn("logDay", dayofmonth(col("logTimestamp")))
        )

    def addWatermark(df):
        return (
            df
                .withWatermark("logTimestamp", "30 days")
        )

    def logDayAggregation(df):
        return (
            df
                .groupBy(
                    window(col("logTimestamp"), "30 days", "30 days"),
                    col("logDay")
                 )
                .count()
                .select(
                    col("logDay"),
                    col("count").alias("eventCount")
                )
        )

class Pipeline:

    def extract(spark, read_path):
        bronzeDF = (
            spark
                .readStream
                .schema(SubmitScoresTypes.tableBronzeType)
                .parquet(read_path)
        )

        return bronzeDF


    def transform(spark, df):
        dateColsDF = Transformations.addDateColumns(df)
        watermarkedDF = Transformations.addWatermark(dateColsDF)
        aggDF = Transformations.logDayAggregation(watermarkedDF)

        return aggDF


    def load(spark, write_path_stream, write_path_checkpoint, df):
        writer = (
            df
                .writeStream
                .queryName("bronzeStream")
                .format("parquet")
                .option("checkpointLocation", write_path_checkpoint)
                .option("path", write_path_stream)
                .start()
        )

        writer.awaitTermination(30)
        writer.stop()


def main(_read_path, _write_path_stream, _write_path_checkpoint):
    spark = SparkSession.builder.getOrCreate()

    bronzeDF = extract(spark, _read_path)
    transformedDF = transform(spark, bronzeDF)
    load(spark, _write_path_stream, _write_path_checkpoint, transformedDF)






userhome = "dbfs:/user/Michael.Paris@act.org"
bronzeTable = "/sensorDataEvents2019Nov.parquet"
silverTable = "/silverDataEvents2019Nov.parquet"
streamCheckpointDir = "/silverCheckpoint"

READ_PATH = userhome+bronzeTable
STREAM_WRITE_PATH = userhome+silverTable
STREAM_CHECKPOINT_PATH = userhome+streamCheckpointDir



