from pyspark.sql.types import StructType, StructField, StringType, ArrayType, TimestampType, FloatType


class RADTypes:
  logSchema = StructType([
    StructField("body", StringType(), True)
  ])

  TimestampType = TimestampType
  

class SubmitScoresTypes:

  # ---- nested sensorDataEvent obejcts ----
  actorStruct = StructType([
    StructField("id", StringType(), True),
    StructField("type", StringType(), True)
  ])

  objectStruct = StructType([
    StructField("id", StringType(), True),
    StructField("type", StringType(), True),
    StructField("name", StringType(), True),
    StructField("learningObjectives", ArrayType(StringType()))
  ])

  groupStruct = StructType([
    StructField("id", StringType(), True),
    StructField("type", StringType(), True),
    StructField("name", StringType(), True)
  ])

  generatedStruct = StructType([
    StructField("id", StringType(), True),
    StructField("type", StringType(), True),
    StructField("startedAtTime", TimestampType(), True),
    StructField("endedAtTime", TimestampType(), True),
    #StructField("value", StringType(), True),
    StructField("score", FloatType(), True)
  ])

  # --------------





  sensorDataEventTypeFields = [
    StructField("id", StringType(), True),
    StructField("actor", actorStruct, True),
    StructField("type", StringType(), True),
    StructField("object", objectStruct, True),
    StructField("eventTime", TimestampType(), True),
    StructField("group", groupStruct, True),
    StructField("generated", generatedStruct, True)

  ]
  sensorDataEventType = StructType(sensorDataEventTypeFields)



  sensorEventType = StructType([
    StructField("sensor", StringType(), True),
    StructField("sendTime", TimestampType(), True),
    StructField("dataVersion", StringType(), True),
    StructField("data", ArrayType(sensorDataEventType))

  ])



  sensorEventArrayType = ArrayType(sensorEventType) 



  # ------------- Bronze Table ---------------------

  tableBronzeType = StructType(
    [
      StructField("logTimestamp", TimestampType(), True),
      StructField("sendTime", TimestampType(), True),
    ] +
    sensorDataEventTypeFields 
    
  )
  



