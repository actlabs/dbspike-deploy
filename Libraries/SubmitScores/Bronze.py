from pyspark.sql import SparkSession
from pyspark.sql.functions import split, col, explode, size, from_json

import os

from .StructTypes import RADTypes, SubmitScoresTypes

  
class Transformations:

    def splitRADEvents(df):
        return (
            df
            .select(
                "*", 
                split( col("value"), "RAD event:").alias('split')
            )
        )

    def getRADEvents(df):
        return (
            df
            .filter( size('split') >= 2)
            .select(
                split( col("split")[0], " " )[0].cast(RADTypes.TimestampType()).alias('logTimestamp') ,
                col("split")[1].alias("logMessage")
            )  
        )

    def getRADLog(df):
        return (
            df
            .select(
                col("logTimestamp"),
                from_json( col("logMessage"), RADTypes.logSchema ).alias("logJSON")
            )
        )

    def getSensorEventList(df):
        return (
            df
            .select(
                col("logTimestamp"),
                from_json( 
                col("logJSON").body, SubmitScoresTypes.sensorEventArrayType 
                ).alias("sensorEventList")        
            )
        )

    def flattenSensorEventList(df):
        return (
            df
            .select(
                col("logTimestamp"),
                explode("sensorEventList").alias("sensorEvent")  
            ) 
        )

    def flattenSensorDataEventList(df):
        return(
            df
            .select(
                col("logTimestamp"),
                col("sensorEvent.sendTime").alias("sendTime"),
                explode("sensorEvent.data").alias("sensorDataEvent")
            )
        )

    def flattenSensorDataEventStruct(df):
        return (
            df
            .select(
                col("logTimestamp"),
                col("sendTime"),
                col("sensorDataEvent.id").alias("id"),
                col("sensorDataEvent.actor").alias("actor"),
                col("sensorDataEvent.type").alias("type"),
                col("sensorDataEvent.object").alias("object"),
                col("sensorDataEvent.eventTime").alias("eventTime"),
                col("sensorDataEvent.group").alias("group"),
                col("sensorDataEvent.generated").alias("generated"),   
            )
        )



class Pipeline:


    def extract(spark, mnt_path):
        allLogStreams = os.path.join(mnt_path, '*/*')
        raw = spark.read.text(allLogStreams)
        return raw

    def transform(spark, rawDF):
        # event filter
        splitEventDF = Transformations.splitRADEvents(rawDF)
        radEventsDF = Transformations.getRADEvents(splitEventDF)

        # get the log objects
        logDF = Transformations.getRADLog(radEventsDF)

        # get the sensorEvent List from log
        sensorEventListDF = Transformations.getSensorEventList(logDF)

        # flatten the sensorEvent List
        sensorEventsFlatDF = Transformations.flattenSensorEventList(sensorEventListDF)

        # flatten sensorDataEvent List from the sensorEvents
        sensorDataEventsFlatDF = Transformations.flattenSensorDataEventList(sensorEventsFlatDF)

        # flatten sensorDataEvent object into Dataframe columns
        sensorDataEventsFlatStructDF = Transformations.flattenSensorDataEventStruct(sensorDataEventsFlatDF)

        return sensorDataEventsFlatStructDF

    def load(spark, write_path, writeDF):
        coalescedDF = writeDF.coalesce(2*4)
        coalescedDF.write.mode("OVERWRITE").parquet(write_path)


def main(_mnt_path, _write_path):
    spark = SparkSession.builder.getOrCreate()

    rawDF = Pipeline.extract(spark, _mnt_path)
    transformedDF = Pipeline.transform(spark, rawDF)
    Pipeline.load(spark, _write_path, transformedDF)

