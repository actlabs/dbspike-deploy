import os 

VERSION = os.getenv('BITBUCKET_BUILD_NUMBER')
from setuptools import setup


setup(name='sdlcSpike',
      version="v{}".format(VERSION),
      description='Sample Pyspark Application for use with Databricks Conenct',
      author='ACTNext',
      author_email='Michael.Paris@act.org',
      packages=['SubmitScores'],
      zip_safe=False
      )

