# Bitbucket Pipelines supplied
#export BITBUCKET_DEPLOYMENT_ENVIRONMENT=int
#export BITBUCKET_BUILD_NUMBER=0

#Repository Variables
#export DATABRICKS_API_TOKEN=$(cat ~/.databricks-connect | jq .token)
#export DATABRICKS_CLUSTER_ID_=
#export DATABRICKS_CLUSTER_ID_int=0213-194530-water3
#export DATABRICKS_CLUSTER_ID_test=0213-194530-water3
#export DATABRICKS_CLUSTER_ID_prod=0213-194530-water3



clusterref="DATABRICKS_CLUSTER_ID_$BITBUCKET_DEPLOYMENT_ENVIRONMENT"
export DATABRICKS_CLUSTER_ID=${!clusterref}
export DATABRICKS_ADDRESS=https://actinc.cloud.databricks.com
export DATABRICKS_ORG_ID=0
export DATABRICKS_PORT=15001

export SCRIPTPATH=Automation/Deployments
export NOTEBOOKPATH=Workspace/
export LIBRARYPATH=Libraries
export BUILDPATH=Builds/$BITBUCKET_BUILD_NUMBER
export OUTFILEPATH=$BUILDPATH/Validation/Output
export TESTRESULTPATH=$BUILDPATH/Validation/junit
export WORKSPACEPATH=/Shared/spikeSDLC/$BITBUCKET_DEPLOYMENT_ENVIRONMENT/
export DBFSPATH=dbfs:/Shared/spikeSDLC/$BITBUCKET_DEPLOYMENT_ENVIRONMENT/Wheels/

export TABLEDIR=dbfs:/Shared/spikeSDLC/$BITBUCKET_DEPLOYMENT_ENVIRONMENT/Tables/




