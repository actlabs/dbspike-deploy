# Use Databricks CLI to deploy notebooks
databricks workspace import_dir -o $BUILDPATH/Workspace $WORKSPACEPATH

# copy built libraries into dbfs
dbfs cp -r $BUILDPATH/Libraries $DBFSPATH

#Space delimited list of libraries
LIBS=$(find $BUILDPATH/Libraries -name '*.whl'| sed 's#.*/##' | tr "\n" " " )
echo "found libs "$LIBS

# Script to uninstall reboot if needed and install library
python $SCRIPTPATH/installWhlLibrary.py \
    --workspace=$DATABRICKS_ADDRESS \
    --token=$DATABRICKS_API_TOKEN \
    --cluster=$DATABRICKS_CLUSTER_ID \
    --libs=$LIBS \
    --dbfspath=$DBFSPATH

