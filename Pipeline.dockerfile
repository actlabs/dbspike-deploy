FROM databricksruntime/python


# COPY ./requirements.txt /tmp/requirements.txt
# COPY ./.databricks-connect /root/.databricks-connect
# COPY ./.databrickscfg /root/.databrickscfg
# COPY ./secretreplace.sh /root/secretreplace.sh
# COPY ./pipelines /root/pipelines/
# COPY ./tests /root/tests



# RUN /bin/bash -c "/databricks/conda/bin/conda init ; \
#     /databricks/conda/bin/conda create --name dbconnect python=3.7 ; \
#     /databricks/conda/bin/conda env list ; \ 
#     "
#RUN /databricks/conda/bin/conda init 
#RUN conda env create --file /tmp/environment.yml

# RUN source /root/.bashrc
#RUN /bin/bash -c 'source /root/.bashrc' 
#RUN /bin/bash -c "conda activate dcs-minimal"
#RUN /bin/bash -c "pip uninstall pyspark"
#RUN /bin/bash


COPY dbconnect_env.yml /databricks/.conda-env-def/dbconnect_env.yml

RUN /databricks/conda/bin/conda env create --file /databricks/.conda-env-def/dbconnect_env.yml 
    # Source conda.sh for all login shells.
    #&& ln -s /databricks/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh

# Conda recommends using strict channel priority speed up conda operations and reduce package incompatibility problems.
# Set always_yes to avoid needing -y flags, and improve conda experience in Databricks notebooks.
RUN /databricks/conda/bin/conda config --system --set channel_priority strict \
    && /databricks/conda/bin/conda config --system --set always_yes True

# This environment variable must be set to indicate the conda environment to activate.
# Note that currently, we have to set both of these environment variables. The first one is necessary to indicate that this runtime supports conda.
# The second one is necessary so that the python notebook/repl can be started (won't work without it)
ENV DEFAULT_DATABRICKS_ROOT_CONDA_ENV=dbconnect
ENV DATABRICKS_ROOT_CONDA_ENV=dbconnect