from pipelines.jobs import submitScoresSilver2019
from pyspark.sql import SparkSession




userhome = "dbfs:/user/Michael.Paris@act.org"
bronzeTable = "/sensorDataEvents2019Nov.parquet"
silverTable = "/silverDataEvents2019Nov.parquet"
streamCheckpointDir = "/silverCheckpoint"

READ_PATH = userhome+bronzeTable
STREAM_WRITE_PATH = userhome+silverTable
STREAM_CHECKPOINT_PATH = userhome+streamCheckpointDir

submitScoresSilver2019.main(READ_PATH, STREAM_WRITE_PATH, STREAM_CHECKPOINT_PATH)
