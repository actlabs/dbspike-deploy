mkdir -p $BUILDPATH/Workspace
mkdir -p $BUILDPATH/Libraries
mkdir -p $BUILDPATH/Validation/Output


# Get notebooks
cp -r $NOTEBOOKPATH $BUILDPATH

# Get packaged libs
find $LIBRARYPATH -name '*.whl' | xargs -I '{}' cp '{}' $BUILDPATH/Libraries


#Generate artifact
tar -czvf Builds/latest_build.tar.gz $BUILDPATH