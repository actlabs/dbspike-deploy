POC for developing Spark ETL batch/streaming jobs locally and deploying as library into Databricks


Pipeline and scripts are derived from this databricks blog article
https://docs.databricks.com/dev-tools/ci-cd.html







## Setup Databricks Connect
Configure databricks connect to target res-gp-dev cluster
[dbconnect](https://docs.databricks.com/dev-tools/databricks-connect.html)



# Executions

## Local 
`(dbconnect) python executeBronze.py`

The executorBronze script will call the `main()` method from the submitScoresBronze2019.py code with parameters specifying things like data source and sink paths


executeSilver.py is structured the same, but will not work locally because the `extract()` and `load()` methods use Streaming objects.


The executor scripts are specified in the job JSON as the 'entrypoints' to a pipeline

## Cluster 
Can build the Wheel and install as library to the cluster.
In notebook on the cluster, can import the library and use functions

`from pipelines.functions.submitScoresSilverTransformations import addDateColumns`

## Jobs

After building the Wheel, `deploy.sh` uploads the Wheel to dbfs:/ and creates a Job.
The uploaded Wheel is declared as dependency to the job, and the executor file is specified as the MainScript to the job.








# Testing

## Unit
Using `pytest`.

Testing Dataframe transformations prior to building the wheel artifact

Loads local, version controlled, fixtures into Spark context.
Executes transformations from the respective 'jobs' on fixtured DF and checks the result

## Debugging

Can debug pytests in VScode with `<insert breakpoint>` `<open command pallette>`  `Python: Debug all tests`
The only configuration needed should already be in the repos .vscode/settings.json




# Local Docker

Wanted to replicate the env that Bitbucket Pipelines uses and debug it locally 

Assuming you have docker daemon installed;


## .localenvs
The file `.localenvs` declares environment variables that `secretreplace.sh` will sed placeholders in `.databricks-connect` and `.databrickscfg` with. As well as be used in other deployment scripts.

The databricks documentation claims that databricks-connect cli will look for environemnt variables, but I've  been having to explicitly define them in the `.databricks-connect` config

The commented lines are identical to Repository Variables provided by Bitbucket Pipelines and must be uncommented when running scripts locally or in docker container

run `git update-index --assume-unchanged .localenvs` if you end up editing the file

## Pipeline.dockerfile
There's probably a way to make conda work with Dockerfiles

## env.list
Provides env variables declared in `.localenvs` to the Docker container

## steps

`source .localenvs`

`docker build -f ./Pipeline.dockerfile . -t dbspike-deploy-local`

`docker run --env-file env.list -it dbspike-deploy-local`

`conda activate dbconnect`

`pip install -r /tmp/requirements.txt`

`cd && ./secretreplace.sh`

`databricks-connect test`

Run unit tests with

`cd`
`pytest`




# Credit 
Pulled a lot from this repo 
https://github.com/DataThirstLtd/Databricks-Connect-PySpark

